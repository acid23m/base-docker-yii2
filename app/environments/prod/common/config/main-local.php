<?php

use yii\db\Connection;

// env settings
$dotenv = new \Dotenv\Dotenv(dirname(__DIR__, 2));
$dotenv->load();

$db_name = \getenv('DB_NAME_PROD');
$db_user = \getenv('DB_USER');
$db_password = \getenv('DB_PASSWORD');

return [
    'components' => [
        'db' => [
            'class' => Connection::class,
//            'dsn' => "mysql:host=db;dbname=$db_name",
//            'username' => 'root',
//            'password' => ''
            'dsn' => "pgsql:host=db;port=5432;dbname=$db_name",
            'username' => $db_user,
            'password' => $db_password
        ],

        'mailer' => [
            'useFileTransport' => false
        ],

        'session' => [
            'timeout' => 86400 // 1 day
        ],

        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => ''
        ]
    ]
];
