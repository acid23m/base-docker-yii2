<?php

use yii\db\Connection;
use yii\gii\generators\controller\Generator as ControllerGenerator;
use yii\gii\generators\crud\Generator as CrudGenerator;
use yii\gii\generators\model\Generator as ModelGenerator;
use yii\gii\generators\module\Generator as ModuleGenerator;
use yii\gii\Module;

// env settings
$dotenv = new \Dotenv\Dotenv(\dirname(__DIR__, 2));
$dotenv->load();

$db_name = \getenv('DB_NAME_DEV');
$db_user = \getenv('DB_USER');
$db_password = \getenv('DB_PASSWORD');

$config = [
    'components' => [
        'db' => [
            'class' => Connection::class,
//            'dsn' => "mysql:host=db;dbname=$db_name",
//            'username' => 'root',
//            'password' => ''
            'dsn' => "pgsql:host=db;port=5432;dbname=$db_name",
            'username' => $db_user,
            'password' => $db_password
        ],

        'mailer' => [
            'useFileTransport' => false
        ],

        'session' => [
            'timeout' => 604800 // 7 days
        ],

        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => ''
        ]
    ]
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => Module::class,
        'allowedIPs' => ['127.0.0.1', '::1'],
        'generators' => [
            'crud' => [
                'class' => CrudGenerator::class,
                'templates' => [
                    'myCrud' => '@vendor/acid23m/yii2-admin/src/gii/templates/crud/my'
                ]
            ],
            'module' => [
                'class' => ModuleGenerator::class,
                'templates' => [
                    'myModule' => '@vendor/acid23m/yii2-admin/src/gii/templates/module/my'
                ]
            ],
            'model' => [
                'class' => ModelGenerator::class,
                'templates' => [
                    'myModel' => '@vendor/acid23m/yii2-admin/src/gii/templates/model/my'
                ]
            ],
            'controller' => [
                'class' => ControllerGenerator::class,
                'templates' => [
                    'myController' => '@vendor/acid23m/yii2-admin/src/gii/templates/controller/my'
                ]
            ]
        ]
    ];
}

return $config;
