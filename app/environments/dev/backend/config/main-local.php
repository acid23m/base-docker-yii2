<?php

use yii\debug\Module;
use yii\queue\debug\Panel;

$config = [
    'components' => [
//        'request' => [
//            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
//            'cookieValidationKey' => '',
//        ],
    ]
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => Module::class,
        'allowedIPs' => ['127.0.0.1', '::1'],
        'panels' => [
            'queue' => Panel::class
        ]
    ];
}

return $config;
