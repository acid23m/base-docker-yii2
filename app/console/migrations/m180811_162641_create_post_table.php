<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post`.
 */
class m180811_162641_create_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_options = null;
        if ($this->getDb()->getDriverName() === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $table_options = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%post}}', [
            'id' => $this->primaryKey()->unsigned(),
            'date' => $this->date()->notNull()->comment('publication date'),
            'title' => $this->string(STRING_LENGTH_LONG)->notNull()->comment('header'),
            'description' => $this->text()->comment('text'),
            'meta_title' => $this->string(STRING_LENGTH_LONG)->notNull()->comment('page title'),
            'meta_description' => $this->string(STRING_LENGTH_LONG)->notNull()->comment('page description'),
            'slug' => $this->string(STRING_LENGTH_LONG)->unique()->notNull()->comment('page slug'),
            'image' => $this->string(STRING_LENGTH_SHORT)->notNull()->comment('image filename'),
            'status' => $this->boolean()->notNull()->defaultValue(true)->comment('active or not'),
            'deleted' => $this->boolean()->notNull()->defaultValue(false)->comment('deleted or not'),
            'position' => $this->integer()->unsigned()->notNull()->defaultValue(1)->comment('sort position'),
            'created_at' => $this->dateTime()->notNull()->defaultExpression('NOW()')->comment('date of creation'),
            'updated_at' => $this->dateTime()->notNull()->defaultExpression('NOW()')->comment('date of modification')
        ], $table_options);

        $this->addCommentOnTable('{{%post}}', 'articles and news');

        $this->createIndex('idx-post-slug', '{{%post}}', 'slug');
        $this->createIndex('idx-post-status', '{{%post}}', 'status');
        $this->createIndex('idx-post-deleted', '{{%post}}', 'deleted');
        $this->createIndex('idx-post-position', '{{%post}}', 'position');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx-post-position', '{{%post}}');
        $this->dropIndex('idx-post-deleted', '{{%post}}');
        $this->dropIndex('idx-post-status', '{{%post}}');
        $this->dropIndex('idx-post-slug', '{{%post}}');

        $this->dropTable('{{%post}}');
    }

}
