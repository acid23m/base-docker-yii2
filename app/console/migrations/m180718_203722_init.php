<?php

use yii\db\Migration;

/**
 * Class m180718_203722_init
 */
class m180718_203722_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $time_zone = '+00:00'; // TODO: change timezone if need it
        switch ($this->db->getDriverName()) {
            case 'mysql':
                $this->execute('SET SQL_MODE = "ANSI_QUOTES";');
                $this->execute('SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";');
                $this->execute('SET SQL_MODE = "TRADITIONAL";');
                $this->execute("SET time_zone = '$time_zone';");
                break;
            case 'pgsql':
                $this->execute("SET TIME ZONE '$time_zone';");
                break;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180718_203722_init cannot be reverted.\n";

        return false;
    }

}
