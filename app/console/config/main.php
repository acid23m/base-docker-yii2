<?php

use dashboard\models\option\rest\Main as MainOption;
use yii\console\controllers\FixtureController;
use yii\console\Request;

$params = \array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-console',
    'basePath' => \dirname(__DIR__),
    'controllerNamespace' => 'console\controllers',

    'bootstrap' => [
        'log',
        'dashboard'
    ],

    'modules' => [
        'dashboard' => [
            'class' => \dashboard\Module::class,
            'controllerNamespace' => 'dashboard\commands',
            /*'user_roles' => [
                'agent' => 'Agent'
            ],
            'user_rules' => static function (
                \yii\rbac\ManagerInterface &$auth,
                array $default_permissions,
                array $default_roles
            ) {
                $receivePayment = $auth->createPermission('receivePayment');
                $receivePayment->description = 'Receive payment';
                $auth->add($receivePayment);

                $agent = $auth->createRole('agent');
                $agent->description = 'Agent';
                $auth->add($agent);
                $auth->addChild($agent, $default_roles[\dashboard\models\user\web\User::ROLE_MODER]);
                $auth->addChild($agent, $default_permissions['isOwner']);
                $auth->addChild($agent, $receivePayment);
            },*/
            'sitemap_items' => [
                'class' => \common\models\sitemap\SitemapCollection::class
            ],
            'search_items' => [
                'class' => \common\models\search\SearchCollection::class
            ]
        ]
    ],

    'controllerMap' => [
        'fixture' => [
            'class' => FixtureController::class,
            'namespace' => 'common\fixtures'
        ]
    ],

    'components' => [
        'request' => [
            'class' => Request::class
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'suffix' => ''
        ],
        'urlManagerFrontend' => require __DIR__ . '/../../frontend/config/url.php',
        'urlManagerBackend' => require __DIR__ . '/../../backend/config/url.php',
        'urlManagerRemote' => require __DIR__ . '/../../remote/config/url.php',

        'option' => [
            'class' => MainOption::class
        ]
    ],

    'params' => $params
];
