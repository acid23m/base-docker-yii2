<?php

namespace console\controllers;

use common\models\post\PostRecord;
use dashboard\models\option\rest\Main as MainOption;
use imagetool\components\Image;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

/**
 * Fills the Database with fake data.
 *
 * @package console\controllers
 * @author Poyarkov S. <webmaster.cipa at gmail dot com>
 */
final class DevDataController extends Controller
{
    /**
     * @var string Auto confirm (yes|no)
     */
    public $force = 'no';

    /**
     * {@inheritdoc}
     */
    public function options($actionID): array
    {
        return ArrayHelper::merge(parent::options($actionID), ['force']);
    }

    /**
     * {@inheritdoc}
     */
    public function optionAliases(): array
    {
        return ArrayHelper::merge(parent::optionAliases(), [
            'f' => 'force'
        ]);
    }

    /**
     * Creates some fake data.
     * @return int
     */
    public function actionIndex(): int
    {
        $numbers = [
            'post' => 2
        ];

        $total = \array_sum(
            \array_values($numbers)
        );
        $done = 0;

        $faker = \Faker\Factory::create('ru_RU');

        Console::startProgress($done, $total);

        // posts
        for ($i = 0; $i < $numbers['post']; $i ++) {
            $this->create(
                PostRecord::class,
                [
                    'date' => $faker->date(),
                    'title' => $faker->realText(50),
                    'description' => $faker->realText(500),
                    'meta_title' => $faker->text(50),
                    'meta_description' => $faker->text()
                ],
                static function (PostRecord $model) use ($faker) {
                    $image = $faker->image();
                    $model->saveImage($image);
                    \unlink($image);
                }
            );
            Console::updateProgress(++ $done, $total);
        }

        Console::endProgress();
        $this->stdout("Done.\n", Console::FG_GREEN);

        return ExitCode::OK;
    }

    /**
     * {@inheritdoc}
     */
    public function beforeAction($action): bool
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        if (!YII_DEBUG) {
            $this->stdout("Available only in Development mode!\n", Console::FG_YELLOW);

            return false;
        }

        switch ($this->force) {
            case 'yes':
                $confirm = true;
                break;
            case 'no':
                $confirm = $this->confirm("\nFlush the database before creating?");
                break;
            default:
                $confirm = false;
        }

        if ($confirm) {
            // re-create database
            \passthru('php /app/yii migrate/fresh --interactive=0');

            // clear images
            $storage_path = \rtrim(\Yii::getAlias(\imagetool\Module::STORAGE_PATH), '/');
            \passthru("rm -rf $storage_path/*");

            // save application logotype
            try {
                $faker = \Faker\Factory::create('ru_RU');
                $tmp_image_path = $faker->image(null, 270, 80, 'abstract');
                $image = new Image($tmp_image_path);
                \chmod($tmp_image_path, 0666);
                $image_base64 = $image->encode(Image::FORMAT_DATA_URI);
                /** @var MainOption $option */
                $option = \Yii::$app->get('option');
                $option->set('site_status', 1);
                $option->set('app_logo', $image_base64);
                $option->validate();
                $option->save();
                \unlink($tmp_image_path);
            } catch (\Exception $e) {
                $this->stdout($e->getMessage() . PHP_EOL, Console::BG_RED);
            }
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);

        // set images rights
        $storage_path = \rtrim(\Yii::getAlias(\imagetool\Module::STORAGE_PATH), '/');
        $user = \getenv('OS_USER_NAME');
        $group = 'www-data';
        \passthru("chown -R $user:$group $storage_path/");

        return $result;
    }

    /**
     * Creates and saves the model.
     * @param string $model_class
     * @param array $attributes Model attributes definition
     * @param \Closure|null $setAttributes Additional definition for model: function(ActiveRecord $model)
     * @return bool
     */
    private function create(string $model_class, array $attributes, ?\Closure $setAttributes = null): bool
    {
        try {
            // create object
            /** @var ActiveRecord $model */
            $model = new $model_class;

            // add parameters
            $model->setAttributes($attributes);
            if ($setAttributes !== null) {
                $setAttributes($model);
            }

            // save model
            return $model->save();
        } catch (\Throwable $e) {
            $this->stdout($e->getMessage() . PHP_EOL, Console::BG_RED);

            return false;
        }
    }

    /**
     * Updates and saves the model.
     * @param string $model_class
     * @param mixed $condition Search model criteria
     * @param array $attributes Model attributes definition
     * @param \Closure|null $setAttributes Additional definition for model: function(ActiveRecord $model)
     * @return bool
     */
    private function update(string $model_class, $condition, array $attributes, ?\Closure $setAttributes = null): bool
    {
        try {
            /** @var ActiveRecord $model */
            $model = $model_class::findOne($condition);
            if ($model === null) {
                $this->stdout("$model_class model is not found.", Console::BG_RED);

                return false;
            }

            // add parameters
            $model->setAttributes($attributes);
            if ($setAttributes !== null) {
                $setAttributes($model);
            }

            // save model
            return $model->save();
        } catch (\Throwable $e) {
            $this->stdout($e->getMessage() . PHP_EOL, Console::BG_RED);

            return false;
        }
    }

}
