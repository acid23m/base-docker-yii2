<?php

namespace backend\widgets;

use yii\base\Widget;

/**
 * Class TopRightPanel.
 *
 * @package backend\widgets
 * @author Poyarkov S. <webmaster.cipa at gmail dot com>
 */
final class TopRightPanel extends Widget
{
    /**
     * {@inheritdoc}
     * @return string
     * @throws \yii\base\InvalidArgumentException
     */
    public function run()
    {
        return $this->render('top_right_panel');
    }

}
