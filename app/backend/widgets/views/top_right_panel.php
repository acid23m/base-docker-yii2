<?php

/** @var \yii\web\View $this */
?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel bg-blue">
            <div class="x_title">
                <h2>
                    <i class="fa fa-circle-o"></i>
                    Site name
                    <small>subheader</small>
                </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="#">Action</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= \Yii::$app->name ?>
            </div>
        </div>
    </div>
</div>
