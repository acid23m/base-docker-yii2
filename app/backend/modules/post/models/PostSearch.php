<?php

namespace backend\modules\post\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PostSearch represents the model behind the search form about `backend\modules\post\models\Post`.
 */
final class PostSearch extends Post
{
    /**
     * {@inheritdoc}
     */
    public function formName(): string
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['id', 'position'], 'integer'],
            [['title', 'date'], 'safe'],
            [['status', 'deleted'], 'boolean']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios(): array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied.
     * @param array $params
     * @return ActiveDataProvider
     * @throws \yii\base\InvalidArgumentException
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = Post::find()->actual();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => ['id', 'position', 'date', 'status', 'deleted'],
                'defaultOrder' => ['position' => SORT_ASC],
                'enableMultiSort' => true
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->date)) {
            if (\strpos($this->date, ',') !== false) {
                [$date_start, $date_end] = \explode(',', $this->date);
                $query->andFilterWhere(['between', '{{%post}}.[[date]]', $date_start, $date_end]);
            } else {
                $query->andFilterWhere(['{{%post}}.[[date]]' => $this->date]);
            }
        }

        $query->andFilterWhere([
            '{{%post}}.[[id]]' => $this->id,
            '{{%post}}.[[status]]' => $this->status,
            '{{%post}}.[[deleted]]' => $this->deleted,
            '{{%post}}.[[position]]' => $this->position
        ]);

        $query->andFilterWhere([LIKE, '{{%post}}.[[title]]', $this->title]);

        return $dataProvider;
    }

}
