<?php

namespace backend\modules\post\models;

use common\models\post\PostRecord;
use dashboard\traits\Model;
use imagetool\helpers\File;
use yii\base\InvalidArgumentException;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * Class Post.
 *
 * @package backend\modules\post\models
 * @author Poyarkov S. <webmaster.cipa at gmail dot com>
 */
class Post extends PostRecord
{
    use Model;

    /**
     * @var null|UploadedFile
     */
    public $image_file;
    /**
     * @var array
     */
    protected $statuses;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        $this->statuses = [
            self::STATUS_PUBLISHED => \Yii::t('dashboard', 'opublikovana'),
            self::STATUS_DRAFT => \Yii::t('dashboard', 'chernovik')
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        $rules = [
            [
                ['image_file'],
                'image',
                'skipOnEmpty' => !$this->isNewRecord,
                'mimeTypes' => ['image/*'],
                'maxSize' => self::IMAGE_MAX_SIZE
            ]
        ];

        return ArrayHelper::merge(parent::rules(), $rules);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'date' => \Yii::t('dashboard', 'data'),
            'title' => \Yii::t('dashboard', 'zagolovok'),
            'slug' => \Yii::t('dashboard', 'slag'),
            'description' => \Yii::t('dashboard', 'opisanie'),
            'image' => \Yii::t('dashboard', 'izobrazhenie'),
            'image_file' => \Yii::t('dashboard', 'izobrazhenie'),
            'meta_title' => \Yii::t('dashboard', 'zagolovok stranici'),
            'meta_description' => \Yii::t('dashboard', 'opisanie stranici'),
            'status' => \Yii::t('dashboard', 'status'),
            'deleted' => \Yii::t('dashboard', 'udalena'),
            'position' => \Yii::t('dashboard', 'pozicia'),
            'created_at' => \Yii::t('dashboard', 'vremya sozdaniya'),
            'updated_at' => \Yii::t('dashboard', 'vremya obnovleniya')
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeValidate(): bool
    {
        $this->image_file = UploadedFile::getInstance($this, 'image_file');

        return parent::beforeValidate();
    }

    /**
     * {@inheritdoc}
     * @param $insert
     * @return bool
     * @throws InvalidArgumentException
     * @throws \ImageOptimizer\Exception\Exception
     * @throws \Intervention\Image\Exception\NotWritableException
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert): bool
    {
        if (parent::beforeSave($insert)) {
            // image
            if ($this->image_file !== null) {
                // remove old image on update
                if (!$insert) {
                    File::delete($this->image);
                    $this->image = null;
                }

                // reformat image
                $this->saveImage($this->image_file);
            }

            return true;
        }

        return false;
    }

}
