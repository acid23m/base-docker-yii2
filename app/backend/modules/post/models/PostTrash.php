<?php

namespace backend\modules\post\models;

use common\models\post\PostRecord;
use dashboard\models\trash\TrashableInterface;
use yii\helpers\Url;

/**
 * Class PostTrash.
 *
 * @package backend\modules\post\models
 * @author Poyarkov S. <webmaster.cipa at gmail dot com>
 */
final class PostTrash extends PostRecord implements TrashableInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getDeleteAttribute(): string
    {
        return 'deleted';
    }

    /**
     * {@inheritdoc}
     */
    public function getOwnerLabel(): string
    {
        return 'Статьи';
    }

    /**
     * {@inheritdoc}
     */
    public function getItemLabel(): string
    {
        return $this->title;
    }

    /**
     * {@inheritdoc}
     * @throws \yii\base\InvalidArgumentException
     */
    public function getViewUrl(): string
    {
        return Url::to(['/post/item/view', 'id' => $this->id]);
    }

}
