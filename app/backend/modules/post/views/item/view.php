<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var backend\modules\post\models\Post $model */

$this->title = $model->title;
$this->params['title'] = $this->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$status_list = $model->getList('statuses');
?>

<div class="post-view">
    <div class="row">
        <div class="col-xs-12">

            <p>
                <?= Html::a(\Yii::t('yii', 'Update'), ['update', 'id' => $model->id], [
                    'class' => 'btn btn-primary js_show_progress'
                ]) ?>
                <?= Html::a(\Yii::t('yii', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => \Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'method' => 'post'
                    ]
                ]) ?>
                <?php if ($model->deleted === $model::STATUS_DELETED): ?>
                    <?= Html::a(\Yii::t('dashboard', 'vosstanovit'), ['restore', 'id' => $model->id], [
                        'class' => 'btn btn-default js_show_progress'
                    ]) ?>
                <?php endif ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'date:date',
                    [
                        'attribute' => 'title',
                        'format' => 'html',
                        'value' => Html::tag('strong', $model->title)
                    ],
                    'description:raw',
                    [
                        'attribute' => 'image',
                        'format' => 'raw',
                        'value' => !empty($model->image) && $model->image !== null
                            ? \imagetool\helpers\Html::img($model->image, [
                                'class' => 'img-responsive',
                                'width' => 300,
                                'quality' => 50
                            ])
                            : null
                    ],
                    [
                        'attribute' => 'slug',
                        'format' => 'html',
                        'value' => Html::a(
                            $model->slug,
                            \Yii::$app->get('urlManagerFrontend')->createUrl(['post/view', 'slug' => $model->slug])
                        )
                    ],
                    'meta_title',
                    'meta_description',
                    [
                        'attribute' => 'status',
                        'value' => $status_list()[$model->status]
                    ],
                    'deleted:boolean',
                    'position',
                    [
                        'attribute' => 'created_at',
                        'format' => 'datetime',
                        'value' => $model::toLocalTimezone($model->created_at)
                    ],
                    [
                        'attribute' => 'updated_at',
                        'format' => 'datetime',
                        'value' => $model::toLocalTimezone($model->updated_at)
                    ]
                ]
            ]) ?>

        </div>
    </div>
</div>
