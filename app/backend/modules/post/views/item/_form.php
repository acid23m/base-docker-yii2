<?php

use imagetool\helpers\File;
use kartik\datecontrol\DateControl;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \backend\modules\post\models\Post $model */
/** @var ActiveForm $form */

$status_list = $model->getList('statuses');
?>

<div class="post-form">
    <div class="row">
        <div class="col-xs-12">

            <div class="panel panel-default">
                <?php $form = ActiveForm::begin() ?>

                <div class="panel-body">
                    <?php $form->errorSummary($model) ?>


                    <div class="row">
                        <div class="col-xs-12 col-md-8">
                            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                        </div>

                        <div class="col-xs-12 col-md-4">
                            <?= $form->field($model, 'date')->widget(DateControl::class, [
                                'type' => DateControl::FORMAT_DATE,
                                'displayTimezone' => \Yii::$app->getTimeZone()
                            ]) ?>
                        </div>
                    </div>


                    <?= $form->field($model, 'description')->widget(CKEditor::class, [
                        'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                            'preset' => 'standart',
                            'height' => 400,
                            'resize_enabled' => true,
                            'resize_dir' => 'vertical',
                            'toolbarCanCollapse' => true
                        ])
                    ]) ?>


                    <?= $form->field($model, 'image_file')->widget(FileInput::class, [
                        'options' => [
                            'accept' => 'image/*'
                        ],
                        'pluginOptions' => [
                            'previewFileType' => 'image',
                            'showUpload' => false,
                            'browseClass' => 'btn btn-default',
                            'initialPreview' => empty($model->image) || $model->image === null
                                ? []
                                : [File::getUrl($model->image)],
                            'initialPreviewAsData' => true,
                            'overwriteInitial' => false
                        ]
                    ]) ?>


                    <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>


                    <?= $form->field($model, 'meta_description')->textInput(['maxlength' => true]) ?>


                    <div class="row">
                        <div class="col-xs-12 col-md-4">
                            <?php $model->status = (int) $model->status ?>
                            <?= $form->field($model, 'status')->dropDownList($status_list()) ?>
                        </div>
                    </div>

                </div>

                <div class="panel-footer">
                    <?= Html::submitButton($model->isNewRecord
                        ? \Yii::t('dashboard', 'sozdat')
                        : \Yii::t('dashboard', 'sohranit'), [
                            'class' => $model->isNewRecord
                                ? 'btn btn-success js_show_progress'
                                : 'btn btn-primary js_show_progress'
                        ]
                    ) ?>
                </div>

                <?php ActiveForm::end() ?>
            </div>

        </div>
    </div>
</div>
