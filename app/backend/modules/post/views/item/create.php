<?php

/** @var \yii\web\View $this */
/** @var \backend\modules\post\models\Post $model */

$this->title = \Yii::t('dashboard', 'dobavit zapis');
$this->params['title'] = $this->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="post-create">

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
