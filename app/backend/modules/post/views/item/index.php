<?php

use backend\modules\post\models\Post;
use kartik\daterange\DateRangePicker;
use kartik\sortable\Sortable;
use yii\grid\ActionColumn;
use yii\grid\CheckboxColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yiister\gentelella\widgets\grid\GridView;

/** @var \yii\web\View $this */
/** @var \backend\modules\post\models\PostSearch $searchModel */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var array $sortItems */

$this->title = 'Posts';
$this->params['title'] = $this->title;
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs('
    (function ($) {
        "use strict";

        const $grid = $("#post-grid");
        const $delBtn = $("#del-multi-btn");
        
        $grid.on("change", "input[name^=selection]", () => {
            let keys = $grid.yiiGridView("getSelectedRows");

            if (keys.length > 0) {
                $delBtn
                    .removeAttr("disabled")
                    .attr("href", "' . Url::to(['delete-multiple']) . '?ids=" + JSON.stringify(keys));
            } else {
                $delBtn.attr("disabled", "disabled");
            }
        });
    })(jQuery);
', $this::POS_END);

\backend\assets\HolderAsset::register($this);
\backend\assets\LazysizesAsset::register($this);

$status_list = $searchModel->getList('statuses');
?>

<div class="post-index">
    <div class="row">
        <div class="col-xs-12">

            <?php // echo $this->render('_search', ['model' => $searchModel]) ?>

            <p>
                <?= Html::a(\Yii::t('dashboard', 'dobavit zapis'), ['create'], [
                    'class' => 'btn btn-success js_show_progress'
                ]) ?>
                <?= Html::a(\Yii::t('dashboard', 'udalit otmechennie'), ['delete-multiple'], [
                    'id' => 'del-multi-btn',
                    'class' => 'btn btn-danger',
                    'disabled' => 'disabled',
                    'data' => [
                        'confirm' => \Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                        'pjax' => 0
                    ]
                ]) ?>
            </p>

            <?= GridView::widget([
                'id' => 'post-grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'condensed' => true,
                'columns' => [
                    ['class' => CheckboxColumn::class],
//                    ['class' => SerialColumn::class],

                    [
                        'attribute' => 'position',
                        'format' => 'integer',
                        'contentOptions' => ['style' => 'width: 75px;']
                    ],
                    [
                        'attribute' => 'image',
                        'format' => 'raw',
                        'value' => static function (Post $model, $key, $index) {
                            /*return !empty($model->image) && $model->image !== null
                                ? \imagetool\helpers\Html::img($model->image, [
                                    'class' => 'img-responsive',
                                    'width' => 200,
                                    'quality' => 30
                                ])
                                : \Yii::$app->params['no-image-tag'];*/
                            if (empty($model->image) || $model->image === null) {
                                return \Yii::$app->params['no-image-tag'];
                            }

                            $image = \imagetool\helpers\File::getUrl($model->image, [
                                'w' => 200,
                                'q' => 30
                            ]);

                            return "<img class='lazyload' data-src='$image' data-sizes='auto' alt=''>";
                        },
                        'contentOptions' => ['style' => 'width: 160px;']
                    ],
                    [
                        'attribute' => 'date',
                        'format' => 'date',
                        'filter' => DateRangePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'date',
                            'convertFormat' => true,
                            'pluginOptions' => [
                                'locale' => [
                                    'format' => 'Y-m-d',
                                    'separator' => ','
                                ]
                            ]
                        ])
                    ],
                    'title',
                    [
                        'attribute' => 'status',
                        'value' => static function (Post $model, $key, $index) use ($status_list) {
                            return $status_list()[$model->status];
                        },
                        'filter' => $status_list()
                    ],
//                    'deleted:boolean',

                    [
                        'class' => ActionColumn::class,
                        'buttonOptions' => ['class' => 'js_show_progress'],
                        'contentOptions' => ['style' => 'width: 90px; text-align: center; vertical-align: middle;']
                    ]
                ]
            ]) ?>

        </div>
    </div>
</div>

<br>

<div class="post-sort">
    <div class="row">
        <div class="col-xs-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= \Yii::t('dashboard', 'sortirovka') ?></h3>
                </div>

                <div class="panel-body">
                    <?php
                    $this->registerCss(<<<'CSS'
.sortable-placeholder,
.sortable-dragging,
.sortable.grid li {
    width: 150px;
    height: 170px;
}
.sortable.grid li {
    float: left;
    overflow: hidden;
    text-align: left;
}
.fig-img {
    height: 80px;
    background-color: #fff;
    overflow: hidden;
}
figcaption {
    margin-top: 10px;
}
CSS
                    );

                    $this->registerJs('let sortData = new Array();', $this::POS_BEGIN);
                    ?>

                    <?php
                    $items = [];

                    /** @var int $pos */
                    /** @var Post $item */
                    foreach ($sortItems as $pos => $item) {
                        $content = '<figure class="model-item" data-id="' . $item->id . '">';
                        $content .= '<div class="fig-img">';
                        if (!empty($item->image) && $item->image !== null) {
                            $content .= \imagetool\helpers\Html::img($item->image, [
                                'class' => 'img-responsive',
                                'width' => 150,
                                'quality' => 30
                            ]);
                        } else {
                            $content .= \Yii::$app->params['no-image-tag'];
                        }
                        $content .= '</div>';
                        $content .= '<figcaption>' . $item->title . '</figcaption>';
                        $content .= '</figure>';

                        $items[] = compact('content');
                    }
                    ?>

                    <?= Sortable::widget([
                        'type' => Sortable::TYPE_GRID,
                        'items' => $items,
                        'options' => ['style' => 'border:none; padding: 0; margin: 0;'],
                        'pluginEvents' => [
                            'sortupdate' => 'function (e, obj) {
                                sortData = [];
                                jQuery(".sortable.grid li").each(function () {
                                    let $item = jQuery(this).find(".model-item"),
                                        id = $item.data("id");
                                        
                                    sortData.push(id);
                                })
                            }'
                        ]
                    ]) ?>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-footer">
                    <?php $this->registerJs('
                    (function ($) {
                        "use strict";
                    
                        $("button[name=save-order]").on("click", e => {
                            e.preventDefault();
                            
                            if (sortData.length !== 0) {
                                window.location = "' . Url::to(['save-order']) . '?ids=" + JSON.stringify(sortData);
                            } else {
                                $("#loading-block").hide();
                            }
                        })
                    })(jQuery);
                    ', $this::POS_END) ?>

                    <?= Html::button(\Yii::t('dashboard', 'sohranit'), [
                        'class' => 'btn btn-primary js_show_progress',
                        'name' => 'save-order'
                    ]) ?>
                </div>
            </div>

        </div>
    </div>
</div>
