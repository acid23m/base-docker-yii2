<?php

/** @var \yii\web\View $this */
/** @var \backend\modules\post\models\Post $model */

$this->title = \Yii::t('dashboard', 'obnovit zapis') . ': ' . $model->title;
$this->params['title'] = $this->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = \Yii::t('yii', 'Update');
?>

<div class="post-update">

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
