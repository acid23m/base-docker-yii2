<?php

namespace backend\modules\post;

use yii\i18n\PhpMessageSource;

/**
 * post module definition class.
 */
final class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\post\controllers';

    /**
     * {@inheritdoc}
     */
    public function init(): void
    {
        parent::init();

        $this->defaultRoute = 'default/index';

        \Yii::$app->i18n->translations["{$this->id}*"] = [
            'class' => PhpMessageSource::class,
            'basePath' => "@backend/modules/{$this->id}/messages"
        ];
    }

}
