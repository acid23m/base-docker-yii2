<?php

namespace backend\modules\post\controllers;

use backend\modules\post\models\Post;
use backend\modules\post\models\PostSearch;
use dashboard\controllers\web\BaseController;
use dashboard\models\user\web\User;
use yii\base\InvalidArgumentException;
use yii\db\IntegrityException;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * ItemController implements the CRUD actions for Post model.
 *
 * @package backend\modules\post\controllers
 */
final class ItemController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();

        $behaviors['access']['rules'] = [
            [
                'allow' => true,
                'actions' => ['index', 'view'],
                'roles' => [User::ROLE_DEMO]
            ],
            [
                'allow' => true,
                'actions' => ['create', 'update', 'delete', 'delete-multiple', 'restore', 'save-order'],
                'roles' => [User::ROLE_MODER]
            ]
        ];

        return $behaviors;
    }

    /**
     * Lists all Post models.
     * @return string
     * @throws InvalidArgumentException
     */
    public function actionIndex(): string
    {
        $searchModel = new PostSearch;
        $dataProvider = $searchModel->search(\Yii::$app->getRequest()->getQueryParams());

        // sort items
        $data = Post::find()->published()->actual()->ordered()->all();
        $sortItems = [];
        foreach ($data as &$item) {
            $sortItems[$item->position] = $item;
        }

        return $this->render('index', \compact('searchModel', 'dataProvider', 'sortItems'));
    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return string
     * @throws InvalidArgumentException
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id): string
    {
        return $this->render('view', [
            'model' => $this->findModel($id)
        ]);
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|Response
     * @throws InvalidArgumentException
     */
    public function actionCreate()
    {
        $model = new Post;

        if ($model->load(\Yii::$app->getRequest()->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('success', \Yii::t('dashboard', 'zapis dobavlena'));

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', \compact('model'));
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id
     * @return string|Response
     * @throws InvalidArgumentException
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(\Yii::$app->getRequest()->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('success', \Yii::t('dashboard', 'zapis obnovlena'));

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', \compact('model'));
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id
     * @return Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id): Response
    {
        $model = $this->findModel($id);

        try {
//            $model->delete();
            $model->softDelete();
        } catch (IntegrityException $e) {
            \Yii::$app->getSession()->setFlash('warning', \Yii::t('dashboard', 'ne udaleno iz za svasannih dannih'));

            return $this->redirect(['view', 'id' => $model->id]);
        } catch (\Throwable $e) {
            \Yii::error($e->getMessage());
            \Yii::$app->getSession()->setFlash('error', 'Error.');

            return $this->redirect(['view', 'id' => $model->id]);
        }

//        \Yii::$app->getSession()->setFlash('success', \Yii::t('dashboard', 'zapis udalena'));
        \Yii::$app->getSession()->setFlash('success', \Yii::t('dashboard', 'zapis v korzine'));

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing Post models.
     * @param string $ids List of ID (array in json format)
     * @return Response
     * @throws InvalidArgumentException
     */
    public function actionDeleteMultiple($ids): Response
    {
        $list = Json::decode($ids);

        try {
//            Post::deleteAll(['id' => $list]);
            $models = Post::find()->where(['id' => $list])->all(); // use beforeDelete and afterDelete
            foreach ($models as $model) {
//                $model->delete();
                $model->softDelete();
            }
        } catch (IntegrityException $e) {
            \Yii::$app->getSession()->setFlash('warning', \Yii::t('dashboard', 'ne udaleno iz za svasannih dannih'));

            return $this->redirect(['index']);
        } catch (\Throwable $e) {
            \Yii::error($e->getMessage());
            \Yii::$app->getSession()->setFlash('error', 'Error.');

            return $this->redirect(['index']);
        }

        \Yii::$app->getSession()->setFlash('success', \Yii::t('dashboard', 'zapisi udaleni'));

        return $this->redirect(['index']);
    }

    /**
     * Changes positions of an existing Post models.
     * @param string $ids List of ID (array in json format)
     * @return Response
     * @throws NotFoundHttpException
     * @throws InvalidArgumentException
     */
    public function actionSaveOrder($ids): Response
    {
        /** @var array $list */
        $list = Json::decode($ids);
        foreach ($list as $index => $id) {
            $model = $this->findModel($id);
            $model->moveToPosition($index + 1); // index is zero based, but position is not
        }

        \Yii::$app->getSession()->setFlash('success', \Yii::t('dashboard', 'zapis obnovlena'));

        return $this->redirect(['index']);
    }

    /**
     * Restores Post model.
     * @param int $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionRestore($id): Response
    {
        $model = $this->findModel($id);
        $model->restore();

        \Yii::$app->getSession()->setFlash('success', \Yii::t('dashboard', 'zapis vosstanovlena'));

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post The loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id): Post
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(\Yii::t('dashboard', 'zapis ne sushestvuet'));
    }

}
