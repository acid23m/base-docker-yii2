<?php

use yii\web\UrlManager;

return [
    'class' => UrlManager::class,
    'baseUrl' => '/admin',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'enableStrictParsing' => false,
    'suffix' => '',
    'rules' => [
        '/' => '/dashboard/home/index'
    ]
];
