<?php

use dashboard\models\option\web\Main as MainOption;
use mihaildev\elfinder\Controller as ElfinderController;
use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\i18n\PhpMessageSource;
use yii\web\JqueryAsset;

$params = \array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => \dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',

    'bootstrap' => [
        'log',
        'option',
        'dashboard'
    ],

    'modules' => [
        'dashboard' => [
            'class' => \dashboard\Module::class,
            'controllerNamespace' => 'dashboard\controllers\web',
            'left_menu' => '@backend/config/menu_left.php',
            'top_menu' => '@backend/config/menu_top.php',
            /*'user_roles' => [
                'agent' => 'Agent'
            ]*/
            /*'top_wide_panel' => '<p class="bg-red" style="padding:10px">Top Wide HTML Content</p>',
            'top_right_panel' => [
                'class' => '\backend\widgets\TopRightPanel'
            ],
            'bottom_left_panel' => '@backend/views/panels/bottom_left_panel.php',
            'bottom_wide_panel' => file_get_contents(__DIR__ . '/../views/panels/bottom_left_panel.php'),*/
            'trash_items' => [
                \backend\modules\post\models\PostTrash::class
            ],
            'sitemap_items' => [
                'class' => \common\models\sitemap\SitemapCollection::class
            ],
            'search_items' => [
                'class' => \common\models\search\SearchCollection::class
            ]
        ],

        'post' => [
            'class' => \backend\modules\post\Module::class
        ]
    ],

    'components' => [
        'urlManager' => require __DIR__ . '/url.php',
        'urlManagerFrontend' => require __DIR__ . '/../../frontend/config/url.php',
        'urlManagerRemote' => require __DIR__ . '/../../remote/config/url.php',

        'request' => [
            'baseUrl' => '/admin',
            'enableCsrfValidation' => true,
            'enableCookieValidation' => true,
            'csrfParam' => '_backendCSRF',
            'csrfCookie' => [
                'secure' => true,
                'httpOnly' => true,
                'path' => '/admin'
            ],
            'ipHeaders' => ['X-Real-Ip', 'X-Forwarded-For']
        ],

        'session' => [
            'name' => 'PHPBACKSESSID',
            'cookieParams' => [
                'path' => '/admin'
            ]
        ],

        'errorHandler' => [
            'errorAction' => '/site/error'
        ],

        'formatter' => [
            'sizeFormatBase' => 1000
        ],

        'assetManager' => [
            'bundles' => [
                JqueryAsset::class => [
                    'sourcePath' => null,
                    'js' => ['//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js']
                ],
                BootstrapAsset::class => [
                    'css' => ['css/bootstrap.min.css']
                ],
                BootstrapPluginAsset::class => [
                    'js' => ['js/bootstrap.min.js']
                ]
            ]
        ],

        'i18n' => [
            'translations' => [
                'top_menu' => [
                    'class' => PhpMessageSource::class,
                    'basePath' => '@backend/messages'
                ]
            ]
        ],

        'option' => [
            'class' => MainOption::class
        ]
    ],

    'controllerMap' => [
        'elfinder' => [
            'class' => ElfinderController::class,
            'access' => ['@'],
            'disabledCommands' => ['netmount'],
            'roots' => [
                [
                    'baseUrl' => '',
                    'basePath' => '@root',
                    'path' => 'userdata/files/uploads',
                    'name' => 'Files'
                ],
                [
                    'baseUrl' => '',
                    'basePath' => '@root',
                    'path' => 'userdata/images/uploads',
                    'name' => 'Images'
                ]
            ]
        ]
    ],

    'params' => $params
];
