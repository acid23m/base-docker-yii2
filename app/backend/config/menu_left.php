<?php

return [
    'Данные' => [
        [
            'label' => 'Статьи',
            'url' => 'javascript:void(0);',
            'icon' => 'th',
            'items' => [
                [
                    'label' => 'Список статей',
                    'url' => ['/post/item/index'],
                    'icon' => 'th',
//                    'badge' => '123',
//                    'badgeOptions' => ['class' => 'label-success']
                ],
                [
                    'label' => 'Добавить статью',
                    'url' => ['/post/item/create'],
                    'icon' => 'plus'
                ]
            ]
        ]
    ]
];
