<?php

namespace backend\assets;

use dashboard\assets\AppAsset;
use yii\web\AssetBundle;

/**
 * Image placeholders.
 *
 * @package backend\assets
 * @author Poyarkov S. <webmaster.cipa at gmail dot com>
 * @link https://github.com/imsky/holder
 */
final class HolderAsset extends AssetBundle
{
    /**
     * {@inheritdoc}
     */
    public $sourcePath = '@bower/holderjs';
    /**
     * {@inheritdoc}
     */
    public $js = [
        'holder.min.js'
    ];
    /**
     * {@inheritdoc}
     */
    public $depends = [
        AppAsset::class
    ];

}
