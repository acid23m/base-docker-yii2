<?php

namespace backend\assets;

use dashboard\assets\AppAsset;
use yii\web\AssetBundle;

/**
 * Lazy load images.
 *
 * @package backend\assets
 * @author Poyarkov S. <webmaster.cipa at gmail dot com>
 * @link https://github.com/aFarkas/lazysizes
 */
final class LazysizesAsset extends AssetBundle
{
    /**
     * {@inheritdoc}
     */
    public $sourcePath = '@bower/lazysizes';
    /**
     * {@inheritdoc}
     */
    public $js = [
        'lazysizes.min.js'
    ];
    /**
     * {@inheritdoc}
     */
    public $depends = [
        AppAsset::class
    ];

}
