<?php

use dashboard\widgets\BodyScript;
use dashboard\widgets\HeadScript;
use dashboard\widgets\Metrica;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\AnalyticGoals;

/** @var \yii\web\View $this */
/** @var string $content */
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="Content-Security-Policy" content="block-all-mixed-content">
    <meta http-equiv="X-DNS-Prefetch-Control" content="on">

    <base href="<?= \rtrim(Url::home(true), '/') ?>">

    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?= HeadScript::widget() ?>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?= $content ?>

<?= BodyScript::widget() ?>
<?= Metrica::widget() ?>
<?php AnalyticGoals::widget([
    'options' => [
//        ['feedback', '.js-feedback-btn', AnalyticGoals::SYSTEM_ALL]
    ]
]) ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
