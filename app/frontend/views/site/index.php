<?php

use multipage\widgets\GeoLocation;

/** @var yii\web\View $this */

$this->title = \Yii::$app->get('option')->get('app_name');

echo GeoLocation::widget([
    'default_city' => 'Tomsk',
    'question' => GeoLocation::ASK_CITY
]);


echo '<div style="display:flex; justify-content: center; align-items: center;">';
echo "<div style='display: flex; justify-content: center; align-items: center; margin-right: 20px;'><span>{$this->title}</span></div>";
echo '<div>' . \imagetool\helpers\Html::img(\Yii::$app->get('option')->get('app_logo')) . '</div>';
echo '</div>';


$posts = \frontend\models\Post::find()->published()->actual()->ordered()->cache(5)->all();

$post_items = [];

foreach ($posts as &$post) {
//    $description = \multipage\models\Process::replaceMarkers($post->description); // CACHE IT !!!

    $html = '<div>';
    $html .= "<h3>{$post->title}</h3>";
    $html .= "<p><small>{$post->date}</small></p>";
    $html .= \imagetool\helpers\Html::img($post->image, ['width' => '150']);
//    $html .= "<div>{$description}</div>";
    $html .= "<div>{$post->description}</div>";
    $html .= '<hr>';
    $html .= '</div>';

    $post_items[] = $html;
}
unset($post, $posts, $html);

echo \yii\helpers\Html::ul($post_items, ['encode' => false]);

//\yii\helpers\VarDumper::dump(\Yii::$app->get(\multipage\Module::GEOIP_COMPONENT_ID)->getIpData('77.243.183.163'), 10, true);
\yii\helpers\VarDumper::dump(\Yii::$app->get(\multipage\Module::GEOIP_COMPONENT_ID)->getIpData('89.20.14.172'), 10, true);
\yii\helpers\VarDumper::dump(\Yii::$app->get(\multipage\Module::GEOIP_COMPONENT_ID)->getIpData('8.8.8.8'), 10, true);
//\Yii::$app->end();
