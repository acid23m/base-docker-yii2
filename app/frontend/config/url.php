<?php

use yii\web\UrlNormalizer;
use yii\web\UrlManager;

return [
    'class' => UrlManager::class,
    'baseUrl' => '',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'enableStrictParsing' => false,
    'suffix' => '',
    'normalizer' => [
        'class' => UrlNormalizer::class,
        'action' => UrlNormalizer::ACTION_REDIRECT_TEMPORARY
    ],
    'rules' => [
        ['pattern' => 'sitemap', 'route' => '/sitemap/index', 'suffix' => '.xml'],
        '/webshell' => '/webshell/default/index',
        '/maintance-mode' => '/maintance-mode/index',

        '/' => '/site/index',
        '<_c:[\w\d\-_]+>' => '<_c>/index',
        '<_c:[\w\d\-_]+>/<_a:[\w\d\-_]+>' => '<_c>/<_a>'
    ]
];