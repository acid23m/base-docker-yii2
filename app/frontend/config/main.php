<?php

use dashboard\models\index\SearchIndex;
use dashboard\models\option\web\Main as MainOption;
use dlds\metas\MetaHandler;

$params = \array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => \dirname(__DIR__),
    'controllerNamespace' => 'frontend\controllers',

    'bootstrap' => [
        'log',
        'option',
        'maintenanceMode'
    ],

    'modules' => [
        \imagetool\Module::DEFAULT_ID => [
            'class' => \imagetool\Module::class,
            'controllerNamespace' => 'imagetool\controllers\web'
        ],
        \multipage\Module::DEFAULT_ID => [
            'class' => \multipage\Module::class,
            'controllerNamespace' => 'multipage\controllers'
        ]
    ],

    'components' => [
        'urlManager' => require __DIR__ . '/url.php',
        'urlManagerBackend' => require __DIR__ . '/../../backend/config/url.php',
        'urlManagerRemote' => require __DIR__ . '/../../remote/config/url.php',

        'request' => [
            'baseUrl' => '',
            'enableCsrfValidation' => true,
            'enableCookieValidation' => true,
            'csrfParam' => '_frontendCSRF',
            'csrfCookie' => [
                'secure' => true,
                'httpOnly' => true,
                'path' => '/'
            ],
            'ipHeaders' => ['X-Real-Ip', 'X-Forwarded-For']
        ],

//        'response' => [
//            'on beforeSend' => static function ($event) {
//                /** @var Response $response */
//                $response = $event->sender;
//                if ($response->data !== null) {
//                    $response->data = \multipage\models\Process::replaceMarkers($response->data);
//                }
//            }
//        ],

        'session' => [
            'name' => 'PHPFRONTSESSID',
            'cookieParams' => [
                'path' => '/'
            ]
        ],

//        'user' => [
//            'identityClass' => 'frontend\modules\client\models\UserIdentity',
//            'enableAutoLogin' => true,
//            'identityCookie' => [
//                'name' => '_frontendUser',
//                'path' => '/',
//                'httpOnly' => true
//            ]
//        ],

        'errorHandler' => [
            'errorAction' => '/site/error',
        ],

        'option' => [
            'class' => MainOption::class
        ],

        'searchIndex' => [
            'class' => SearchIndex::class
        ],

        'metas' => [
            'class' => MetaHandler::class
        ]
    ],

    'params' => $params
];
