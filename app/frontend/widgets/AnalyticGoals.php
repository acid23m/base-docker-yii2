<?php

namespace frontend\widgets;

use yii\base\InvalidArgumentException;
use yii\base\Widget;
use yii\web\View;

/**
 * Class AnalyticGoals.
 *
 * @package frontend\widgets
 * @author Poyarkov S. <webmaster.cipa at gmail dot com>
 */
final class AnalyticGoals extends Widget
{
    public const SYSTEM_ALL = 0;
    public const SYSTEM_YANDEX = 1;
    public const SYSTEM_GOOGLE = 2;

    /**
     * @var array [[(string) goal id, (string) dom selector, (integer) analytic system]]
     */
    public $options;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        if (!\is_array($this->options)) {
            throw new InvalidArgumentException('Options must be the list of arrays [(string) goal id, (string) dom selector, (integer) analytic system].');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function run(): void
    {
        /** @var View $view */
        $view = $this->view;

        try {
            $metrica = new \dashboard\models\option\Metrica;
            $ya_id = $metrica->get('yandex_metrika', '0');
        } catch (\Exception $e) {
            $ya_id = '0';
        }

        foreach ($this->options as [$goal_id, $selector, $system]) {
            if (YII_DEBUG) {
                $ya = 'console.log("ya")';
                $goo = 'console.log("goo")';
            } else {
                $ya = 'try { yaCounter' . $ya_id . '.reachGoal("' . $goal_id . '"); } catch(error) {}';
                $goo = 'try { ga("send", "pageview", "/' . $goal_id . '"); } catch(error) {}';
            }

            switch ($system) {
                case self::SYSTEM_ALL:
                    $events = $ya . PHP_EOL . $goo;
                    break;
                case self::SYSTEM_YANDEX:
                    $events = $ya;
                    break;
                case self::SYSTEM_GOOGLE:
                    $events = $goo;
                    break;
                default:
                    $events = $ya . PHP_EOL . $goo;
            }

            $js = <<<JS
document.querySelectorAll("$selector").forEach(function (item, i, arr) {
    item.addEventListener("click", function (e) {
        //e.preventDefault();
        $events
    }, false);
});
JS;
            $view->registerJs($js, $view::POS_END);
        }
    }

}
