<?php

namespace frontend\controllers;

use dlds\metas\MetaHandler;
use imagetool\helpers\File;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\ErrorAction;

/**
 * Class BaseController.
 *
 * @package frontend\controllers
 * @author Poyarkov S. <webmaster.cipa at gmail dot com>
 */
class BaseController extends Controller
{
    /**
     * @var MetaHandler
     */
    public $metas;

    /**
     * {@inheritdoc}
     */
    public function actions(): array
    {
        $actions = [
            'error' => ErrorAction::class
        ];

        return ArrayHelper::merge(parent::actions(), $actions);
    }

    /**
     * {@inheritdoc}
     */
    public function init(): void
    {
        parent::init();

        $this->metas = \Yii::$app->get('metas');
        $this->metas->type = MetaHandler::T_OG_WEBSITE;
        $this->metas->url = \Yii::$app->getRequest()->getAbsoluteUrl();
        $this->metas->image = File::getUrl(
            \Yii::$app->get('option')->get('app_logo')
        );
        $this->metas->locale = \Yii::$app->language;
//        $this->metas->suffix = false;
        $this->metas->suffix = \Yii::$app->name;
    }

}
