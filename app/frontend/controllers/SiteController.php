<?php

namespace frontend\controllers;

use yii\base\InvalidArgumentException;

/**
 * Site controller
 */
class SiteController extends BaseController
{
    /**
     * Displays homepage.
     * @return string
     * @throws InvalidArgumentException
     */
    public function actionIndex(): string
    {
        $this->metas->title = 'Home Page';
        $this->metas->description = 'My description';

        return $this->render('index');
    }

}
