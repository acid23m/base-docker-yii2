<?php

namespace frontend\models;

use common\models\post\PostRecord;
use multipage\behaviors\ReplaceMarkersBehavior;
use yii\helpers\ArrayHelper;

/**
 * Class Post.
 *
 * @package frontend\models
 * @author Poyarkov S. <webmaster.cipa at gmail dot com>
 */
final class Post extends PostRecord
{
    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        $behaviors = [
            'replaceMarkers' => [
                'class' => ReplaceMarkersBehavior::class,
                'attribute' => 'description'
            ]
        ];

        return ArrayHelper::merge(parent::behaviors(), $behaviors);
    }

}
