<?php

namespace remote\modules\v1;

/**
 * Api version 1.
 *
 * @package remote\modules\v1
 * @author Poyarkov S. <webmaster.cipa at gmail dot com>
 */
final class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'remote\modules\v1\controllers';

    /**
     * {@inheritdoc}
     */
    public function init(): void
    {
        parent::init();

//        $this->defaultRoute = 'default/index';

        $this->modules = [

        ];
    }

}
