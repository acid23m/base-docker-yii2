<?php

namespace remote\controllers;

use yii\helpers\Html;
use yii\rest\Controller;
use yii\web\ErrorAction;
use yii\web\Response;

/**
 * Site controller.
 *
 * @package remote\controllers
 * @author Poyarkov S. <webmaster.cipa at gmail dot com>
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions(): array
    {
        $actions = parent::actions();

        $actions['errors'] = [
            'class' => ErrorAction::class
        ];

        return $actions;
    }

    /**
     * Displays homepage.
     * @return string
     */
    public function actionIndex(): string
    {
        \Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        return Html::encode(\Yii::$app->name);
    }

}
