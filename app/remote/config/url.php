<?php

use yii\web\UrlManager;

return [
    'class' => UrlManager::class,
    'baseUrl' => '/api',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'enableStrictParsing' => true,
    'suffix' => '',
    'rules' => [
        '' => 'site/index',

        'OPTIONS /v1/<_c:[\w\d\-_]+>/<_a:[\w\d\-_]+>' => '/v1/<_c>/options',
        '/v1/<_c:[\w\d\-_]+>/<_a:[\w\d\-_]+>' => '/v1/<_c>/<_a>'
    ]
];
