<?php

use dashboard\models\option\rest\Main as MainOption;
use remote\modules\v1\Module;
use yii\filters\ContentNegotiator;
use yii\web\JsonParser;
use yii\web\JsonResponseFormatter;
use yii\web\Response;

$params = \array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-remote',
    'basePath' => \dirname(__DIR__),
    'controllerNamespace' => 'remote\controllers',

    'bootstrap' => [
        'log',
        'option',
        'dashboard',
        [
            'class' => ContentNegotiator::class,
            'formats' => [
                'application/json' => Response::FORMAT_JSON
            ],
            'languages' => [
                'ru',
                'en'
            ]
        ]
    ],

    'modules' => [
        'v1' => [
            'class' => Module::class
        ],

        'dashboard' => [
            'class' => \dashboard\Module::class,
            'controllerNamespace' => 'dashboard\controllers\rest'
        ]
    ],

    'components' => [
        'request' => [
            'baseUrl' => '/api',
            'enableCsrfValidation' => false,
            'enableCsrfCookie' => false,
            'enableCookieValidation' => true,
            /*'csrfParam' => '_apiCSRF',
            'csrfCookie' => [
                'httpOnly' => true,
                'path' => '/api'
            ],*/
            'parsers' => [
                'application/json' => JsonParser::class
            ],
            'ipHeaders' => ['X-Real-Ip', 'X-Forwarded-For']
        ],

        'response' => [
            'formatters' => [
                Response::FORMAT_JSON => [
                    'class' => JsonResponseFormatter::class,
                    'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
                    'prettyPrint' => YII_DEBUG
                ]
            ],
            'on beforeSend' => static function ($event) {
                /** @var Response $response */
                $response = $event->sender;
                if ($response->data !== null) {
                    $now = new \DateTime('now', new \DateTimeZone(\Yii::$app->timeZone));
                    $response->data = [
                        'success' => $response->isSuccessful,
                        'time' => $now->format(DATE_ATOM),
                        'path' => \Yii::$app->request->getPathInfo(),
                        'method' => \Yii::$app->request->getMethod(),
                        'body' => $response->data
                    ];
                }
            }
        ],

        'urlManager' => require __DIR__ . '/url.php',
        'urlManagerBackend' => require __DIR__ . '/../../backend/config/url.php',
        'urlManagerFrontend' => require __DIR__ . '/../../frontend/config/url.php',

        'session' => [
            'name' => 'PHPPUBSESSID'
        ],

        /*'user' => [
            'identityClass' => 'common\models\access\UserIdentity',
            'enableAutoLogin' => false,
            'enableSession' => false,
            'identityCookie' => [
                'name' => '_apiUser',
                'path' => '/api',
                'httpOnly' => true
            ],
            'loginUrl' => null
        ],*/

        /*'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'db' => 'dbUser'
        ],*/

        'errorHandler' => [
            'errorAction' => '/site/error'
        ],

        'option' => [
            'class' => MainOption::class
        ],
    ],

    'params' => $params
];
