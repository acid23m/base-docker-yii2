This is a skeleton [Yii 2](http://www.yiiframework.com/) application.

The template includes four tiers: front end, back end, rest api and console, each of which
is a separate Yii application.

DIRECTORY STRUCTURE
-------------------

```
common
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
    modules/             contains modules used in both backend and frontend
    data/                contains databases, project files and help files
    tests/               contains tests for common classes    
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
backend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains backend configurations
    controllers/         contains Web controller classes
    models/              contains backend-specific model classes
    modules/             contains modules for backend
    runtime/             contains files generated during runtime
    tests/               contains tests for backend application    
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains backend widgets
frontend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    modules/             contains modules for frontend
    runtime/             contains files generated during runtime
    tests/               contains tests for frontend application
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
remote
    config/              contains api configurations
    controllers/         contains Web controller classes
    models/              contains api-specific model classes
    modules/             contains modules for api
    runtime/             contains files generated during runtime
    tests/               contains tests for api application
vendor/                  contains dependent 3rd-party packages
environments/            contains environment-based overrides
userdata/                contains user-downloaded specific files and images for application
dumps/                   contains database backups
```
