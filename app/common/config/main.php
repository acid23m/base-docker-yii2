<?php

use brussens\maintenance\MaintenanceMode;
use kartik\date\DatePicker;
use kartik\datecontrol\Module as DatecontrolModule;
use kartik\datetime\DateTimePicker;
use yii\db\Connection;
use yii\i18n\PhpMessageSource;
use yii\log\DbTarget;
use yii\log\FileTarget;
use yii\redis\Cache as RedisCache;
use yii\redis\Session as RedisSession;

return [
    'name' => 'My Application',
    'sourceLanguage' => 'sys',
    'language' => 'ru',
//    'timeZone' => 'Asia/Novosibirsk',
//    'timeZone' => 'Europe/Moscow',
    'timeZone' => 'UTC',
    'vendorPath' => dirname(__DIR__, 2) . '/vendor',
    'extensions' => require __DIR__ . '/../../vendor/yiisoft/extensions.php',

    'aliases' => [
        '@root' => dirname(__DIR__, 2),
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
        '@common' => '@root/common',
        '@backend' => '@root/backend',
        '@admin' => '@root/backend',
        '@frontend' => '@root/frontend',
        '@console' => '@root/console',
        '@remote' => '@root/remote',
        '@api' => '@root/remote',
        '@dumps' => '@root/dumps',
        '@userdata' => '@root/userdata'
    ],

    'bootstrap' => [
        'queue'
    ],

    'modules' => [
        'datecontrol' => [
            'class' => DatecontrolModule::class,
            'displaySettings' => [
                DatecontrolModule::FORMAT_DATETIME => 'dd.MM.yyyy hh:mm:ss a',
                DatecontrolModule::FORMAT_DATE => 'dd.MM.yyyy',
                DatecontrolModule::FORMAT_TIME => 'hh:mm:ss a'
            ],
            'saveSettings' => [
                DatecontrolModule::FORMAT_DATETIME => 'php:' . STANDARD_DATETIME_FORMAT,
                DatecontrolModule::FORMAT_DATE => 'php:' . STANDARD_DATE_FORMAT,
                DatecontrolModule::FORMAT_TIME => 'php:H:i:s'
            ],
            'saveTimezone' => 'UTC',
            'autoWidgetSettings' => [
                DatecontrolModule::FORMAT_DATETIME => [
                    'type' => DateTimePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'todayBtn' => true,
                        'todayHighlight' => true
                    ]
                ],
                DatecontrolModule::FORMAT_DATE => [
                    'type' => DatePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'todayBtn' => true,
                        'todayHighlight' => true
                    ]
                ],
                DatecontrolModule::FORMAT_TIME => [
                    'addon' => '',
                    'pluginOptions' => [
                        'minuteStep' => 1,
                        'secondStep' => 5,
                        'showMeridian' => false
                    ]
                ]
            ]
        ]
    ],

    'components' => [
        'db' => [
            'charset' => 'utf8',
            'schemaCacheDuration' => 3600,
            'enableSchemaCache' => !YII_DEBUG,
            'on afterOpen' => static function ($event) {
                /** @var Connection $connection */
                $connection = $event->sender;
                $i = $connection->getDriverName();
                if ($i === 'pgsql') {
                    \define('LIKE', 'ilike');
                } else {
                    \define('LIKE', 'like');
                }
            }
        ],
        'dbRuntime' => [
            'class' => Connection::class,
            'dsn' => 'sqlite:@common/data/runtimedata.db',
            'schemaCacheDuration' => 3600
        ],

        'mailer' => [
            'viewPath' => '@common/mail',
            'htmlLayout' => '@common/mail/layouts/html',
            'textLayout' => '@common/mail/layouts/text',
            'fileTransportPath' => '@app/runtime/email',
            /*'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'localhost',
                'username' => 'username',
                'password' => 'password',
                'port' => '587',
                'encryption' => 'tls'
            ]*/
        ],

        'cache' => [
//            'class' => DbCache::class,
//            'db' => 'dbRuntime'
            'class' => RedisCache::class,
            'redis' => [
                'hostname' => 'cache',
                'port' => 6379,
                'database' => 0
            ],
            'defaultDuration' => 60
        ],

        'session' => [
//            'class' => DbSession::class,
//            'db' => 'dbRuntime',
            'class' => RedisSession::class,
            'redis' => [
                'hostname' => 'cache',
                'port' => 6379,
                'database' => 1
            ],
            'useCookies' => true,
            'cookieParams' => [
                'secure' => true,
                'httpOnly' => true
            ]
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'flushInterval' => 100,
            'targets' => [
                'file' => [
                    'class' => FileTarget::class,
                    'maxLogFiles' => 3,
                    'maxFileSize' => 2048,
                    'levels' => ['error', 'warning'],
                    'except' => YII_DEBUG
                        ? [
                            'yii\web\User::getIdentityAndDurationFromCookie',
                            'yii\debug\Module::checkAccess'
                        ]
                        : [
                            'yii\web\User::getIdentityAndDurationFromCookie',
                            'yii\debug\Module::checkAccess',
                            'yii\web\HttpException:404',
                            'yii\web\HttpException:405'
                        ],
                    'exportInterval' => 100
                ],
                'db' => [
                    'class' => DbTarget::class,
                    'db' => 'dbRuntime',
                    'logTable' => 'log',
                    'levels' => ['error', 'warning'],
                    'except' => YII_DEBUG
                        ? [
                            'yii\web\User::getIdentityAndDurationFromCookie',
                            'yii\debug\Module::checkAccess'
                        ]
                        : [
                            'yii\web\User::getIdentityAndDurationFromCookie',
                            'yii\debug\Module::checkAccess',
                            'yii\web\HttpException:404',
                            'yii\web\HttpException:405'
                        ],
                    'exportInterval' => 100
                ]
            ]
        ],

        'assetManager' => [
            'linkAssets' => !(0 === \stripos(PHP_OS, 'WIN')),
            'appendTimestamp' => YII_DEBUG ? true : false,
            'hashCallback' => static function ($path) {
                return \hash('md4', $path);
            },
            'basePath' => '@webroot/assets',
            'baseUrl' => '@web/assets'
        ],

        'i18n' => [
            'translations' => [
                'yii' => [
                    'class' => PhpMessageSource::class,
                    'basePath' => '@vendor/yiisoft/yii2/messages',
                    'sourceLanguage' => 'en'
                ],
                '*' => [
                    'class' => PhpMessageSource::class,
                    'basePath' => '@app/messages'
                ]
            ]
        ],

        'maintenanceMode' => [
            'class' => MaintenanceMode::class,
            'urls' => [
                'debug/default/toolbar',
                'debug/default/view'
            ],
            'statusCode' => 200,
            'layoutPath' => '@vendor/acid23m/yii2-admin/src/views/layouts/maintenance',
            'viewPath' => '@vendor/acid23m/yii2-admin/src/views/maintenance-mode/index'
        ],

        'queue' => [
            'class' => \yii\queue\beanstalk\Queue::class,
            'as log' => \yii\queue\LogBehavior::class,
            'host' => 'queue'
        ]
    ]

];
