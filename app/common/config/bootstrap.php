<?php
Yii::setAlias('@common', dirname(__DIR__));

const STRING_LENGTH_SHORT = 50;
const STRING_LENGTH_NORMAL = 150;
const STRING_LENGTH_LONG = 255;

const TEXT_LENGTH_NORMAL = 65535;
const TEXT_LENGTH_MEDIUM = 16777215;
const TEXT_LENGTH_LONG = 4294967296;

const STANDARD_DATE_FORMAT = 'Y-m-d';
const STANDARD_DATETIME_FORMAT = 'Y-m-d H:i:s';

const PERM_DIR = 0775;
const PERM_FILE = 0664;

// check runtime data
$runtime_db_file = \Yii::getAlias('@common/data/runtimedata.db');

if (!\file_exists($runtime_db_file)) {
    $runtime_db = new \SQLite3($runtime_db_file);

    $runtime_db->exec(<<<'SQL'
CREATE TABLE "cache" (
    "id" CHAR(128) NOT NULL PRIMARY KEY,
    "expire" INT(11), "data" BLOB
);
SQL
    );

    $runtime_db->exec(<<<'SQL'
CREATE TABLE "log" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    "level" INTEGER,
    "category" VARCHAR(255),
    "log_time" DOUBLE,
    "prefix" TEXT,
    "message" TEXT
);
SQL
    );

    $runtime_db->exec(<<<'SQL'
CREATE TABLE "session" (
    "id" CHAR(255) PRIMARY KEY NOT NULL,
    "expire" INTEGER,
    "data" BLOB
);
SQL
    );

    \chmod($runtime_db_file, PERM_FILE);
}
