<?php

namespace common\models\post;

use dashboard\traits\DateTime;
use imagetool\components\Image;
use imagetool\helpers\File;
use yii\base\InvalidArgumentException;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use yii2tech\ar\position\PositionBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property string $date
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property string $meta_title
 * @property string $meta_description
 * @property string $image
 * @property bool $status
 * @property bool $deleted
 * @property int $position
 * @property string $created_at
 * @property string $updated_at
 *
 * @method void touch(string $attribute) Updates a timestamp attribute to the current timestamp
 *
 * @method bool movePrev() Moves owner record by one position towards the start of the list.
 * @method bool moveNext() Moves owner record by one position towards the end of the list.
 * @method bool moveFirst() Moves owner record to the start of the list.
 * @method bool moveLast() Moves owner record to the end of the list.
 * @method bool moveToPosition(int $position) Moves owner record to the specific position. If specified position exceeds the total number of records, owner will be moved to the end of the list.
 *
 * @method int|false softDelete() Marks the owner as deleted.
 * @method bool beforeSoftDelete() This method is invoked before soft deleting a record.
 * @method void afterSoftDelete() This method is invoked after soft deleting a record.
 * @method int|false restore() Restores record from "deleted" state, after it has been "soft" deleted.
 * @method bool beforeRestore() This method is invoked before record is restored from "deleted" state.
 * @method void afterRestore() This method is invoked after record is restored from "deleted" state.
 * @method int|false safeDelete() Attempts to perform regular [[BaseActiveRecord::delete()]], if it fails with exception, falls back to [[softDelete()]].
 *
 * @package common\models\post
 */
class PostRecord extends ActiveRecord
{
    use DateTime;

    public const STATUS_PUBLISHED = true;
    public const STATUS_DRAFT = false;
    public const STATUS_DELETED = true;
    public const STATUS_NOT_DELETED = false;

    public const IMAGE_MAX_WIDTH = 1200;
    public const IMAGE_MAX_SIZE = 1000000; // 1Mb

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%post}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => [$this, 'getNowUTC']
            ],
            'position' => [
                'class' => PositionBehavior::class,
                'positionAttribute' => 'position'
            ],
            'softDelete' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted' => true
                ],
                'invokeDeleteEvents' => false
            ],
            'slug' => [
                'class' => SluggableBehavior::class,
                'attribute' => ['title', 'date']
                //'ensureUnique' => true,
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['title', 'description', 'meta_title', 'meta_description'], 'trim'],
            [['title', 'date', 'meta_title', 'meta_description'], 'required'],
            [['date'], 'date', 'format' => 'yyyy-MM-dd'],
            [['title', 'slug'], 'string', 'max' => STRING_LENGTH_LONG],
            [['image'], 'string', 'max' => STRING_LENGTH_SHORT],
            [['description'], 'string'],
            [['status', 'deleted'], 'boolean'],
            [['position'], 'integer']
        ];
    }

    /**
     * {@inheritdoc}
     * @return PostQuery the active query used by this AR class.
     */
    public static function find(): PostQuery
    {
        return new PostQuery(static::class);
    }

    /**
     * {@inheritdoc}
     * @throws InvalidArgumentException
     */
    public function beforeDelete(): bool
    {
        if (parent::beforeDelete()) {
            // deletes image
            if (!empty($this->image) && $this->image !== null) {
                File::delete($this->image);
            }

            return true;
        }

        return false;
    }

    /**
     * Save image.
     * @param string|UploadedFile $img
     * @throws \ImageOptimizer\Exception\Exception
     * @throws \Intervention\Image\Exception\NotWritableException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidArgumentException
     */
    public function saveImage($img): void
    {
        if ($img instanceof UploadedFile) { // from POST request
            $img_file = $img->tempName;
            $extension = $img->getExtension();
        } elseif (\is_string($img)) { // path to image file
            $img_file = $img;
            $extension = \pathinfo($img, PATHINFO_EXTENSION);
        } else {
            \Yii::warning('Wrong image type.');

            return;
        }

        $image = new Image($img_file);
        $image->quality = 90;
        $image_2x = clone $image;

        $image->resizeProportional(self::IMAGE_MAX_WIDTH, null);
        $this->image = $image->save($extension);
        unset($image);

        $image_2x->changeDPR(Image::DPR_1X, Image::DPR_2X, false);
        $image_2x->resizeProportional(self::IMAGE_MAX_WIDTH * Image::DPR_2X, null);
        $image_2x->save($extension);
        unset($image_2x);
    }

}
