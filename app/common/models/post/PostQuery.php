<?php

namespace common\models\post;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Post]].
 *
 * @see PostRecord
 */
final class PostQuery extends ActiveQuery
{
    /**
     * Gets published items.
     * @return $this
     */
    public function published(): self
    {
        $this->andWhere(['{{%post}}.[[status]]' => PostRecord::STATUS_PUBLISHED]);

        return $this;
    }

    /**
     * Gets not deleted items.
     * @return $this
     */
    public function actual(): self
    {
        $this->andWhere(['{{%post}}.[[deleted]]' => PostRecord::STATUS_NOT_DELETED]);

        return $this;
    }

    /**
     * Gets deleted items.
     * @return $this
     */
    public function deleted(): self
    {
        $this->andWhere(['{{%post}}.[[deleted]]' => PostRecord::STATUS_DELETED]);

        return $this;
    }

    /**
     * Sorts items by position.
     * @return $this
     */
    public function ordered(): self
    {
        $this->orderBy('{{%post}}.[[position]] ASC');

        return $this;
    }

    /**
     * Shows item by its slug parameter.
     * @param string $slug
     * @return $this
     */
    public function show(string $slug): self
    {
        $this->andWhere(['{{%post}}.[[slug]]' => $slug]);

        return $this;
    }

    /**
     * {@inheritdoc}
     * @return PostRecord[]|array
     */
    public function all($db = null): array
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PostRecord|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}