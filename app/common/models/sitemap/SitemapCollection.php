<?php

namespace common\models\sitemap;

use common\models\post\PostRecord;
use dashboard\models\sitemap\SitemapCollectionInterface;
use InvalidArgumentException;
use samdark\sitemap\Sitemap;
use yii\base\InvalidConfigException;
use yii\web\UrlManager;

/**
 * Sitemap collection.
 *
 * @package common\models\sitemap
 * @author Poyarkov S. <webmaster.cipa at gmail dot com>
 */
final class SitemapCollection implements SitemapCollectionInterface
{
    /**
     * {@inheritdoc}
     * @throws InvalidArgumentException
     * @throws InvalidConfigException
     */
    public function dynamicItems(Sitemap $sitemap): void
    {
        /** @var UrlManager $url_manager */
        $url_manager = \Yii::$app->get('urlManagerFrontend');

        // posts
        $posts_query = PostRecord::find()->actual()->published()->ordered();
        foreach ($posts_query->batch(50) as $posts) {
            /** @var PostRecord $post */
            foreach ($posts as &$post) {
                $sitemap->addItem(
                    $url_manager->createAbsoluteUrl(['post/view', 'slug' => $post->slug]),
                    (new \DateTime($post->updated_at, new \DateTimeZone(\Yii::$app->timeZone)))->format('U'),
                    Sitemap::MONTHLY,
                    0.3
                );
            }
        }
        unset($posts_query, $post);
    }

    /**
     * {@inheritdoc}
     * @throws InvalidArgumentException
     * @throws InvalidConfigException
     */
    public function staticItems(Sitemap $sitemap): void
    {
        /** @var UrlManager $url_manager */
        $url_manager = \Yii::$app->get('urlManagerFrontend');
        $base_url = \rtrim($url_manager->getHostInfo(), '/');

        // home page
        $sitemap->addItem($base_url);
    }

}
