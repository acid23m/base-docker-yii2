<?php

namespace common\models\search;

use common\models\post\PostRecord;
use dashboard\models\index\SearchCollectionInterface;
use S2\Rose\Entity\Indexable;
use S2\Rose\Indexer;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\web\UrlManager;

/**
 * Search index collection.
 *
 * @package common\models\search
 * @author Poyarkov S. <webmaster.cipa at gmail dot com>
 */
class SearchCollection implements SearchCollectionInterface
{
    /**
     * {@inheritdoc}
     */
    public function index(Indexer $indexer): void
    {
        /** @var UrlManager $url_manager_front */
        $url_manager_front = \Yii::$app->get('urlManagerFrontend');
        /** @var UrlManager $url_manager_back */
        $url_manager_back = \Yii::$app->id === 'app-backend'
            ? \Yii::$app->get('urlManager')
            : \Yii::$app->get('urlManagerBackend');

        // posts
        $posts_query = PostRecord::find()->actual()->published()->ordered();
        foreach ($posts_query->batch(50) as $posts) {
            /** @var PostRecord $post */
            foreach ($posts as &$post) {
                $title = Html::encode($post->title);
                $content = $post->description;
                $description = StringHelper::truncateWords($content, 100);
                $date = new \DateTime($post->updated_at, new \DateTimeZone(\Yii::$app->getTimeZone()));

                // for site
                $item = new Indexable(
                    'post-' . $post->id,
                    $title,
                    $content
                );
                $item->setDescription($description );
                $item->setDate($date);
                $item->setUrl(
                    $url_manager_front->createAbsoluteUrl(['/post/view', 'slug' => $post->slug])
                );

                $indexer->index($item);

                // for admin panel
                $item = new Indexable(
                    'post-' . $post->id . '__admin',
                    "[admin panel] $title",
                    $content
                );
                $item->setDescription($description );
                $item->setDate($date);
                $item->setUrl(
                    $url_manager_back->createAbsoluteUrl(['/post/item/view', 'id' => $post->id])
                );

                $indexer->index($item);
            }
        }
        unset($posts_query, $post, $title, $content, $description, $date, $item);
    }

}
